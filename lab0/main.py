import numpy as np

if __name__ == '__main__':
    ms = 10
    matrix = np.ones((ms, ms))
    matrix[1:ms - 1, 1:ms - 1] = 0
    print(matrix)
